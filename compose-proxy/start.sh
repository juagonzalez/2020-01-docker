#!/bin/sh

if [ $# -lt 1 ] ; then
	echo "Usage: ./start.sh <PORT>"
	exit 1
fi

export SERVICES_PORT=$1
docker-compose up -d
